const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  /*关闭每次保存都进行检测*/
  lintOnSave: false,
  /*项目部署 路径*/
  publicPath: "/",
  transpileDependencies: true,
  // 设为false打包时不生成.map文件
  productionSourceMap: false,
  devServer: {
    /*自动打开浏览器*/
    open: true,
    host: "localhost",
    port: 8081,
    https: false,
    /*配置跨域*/
    proxy: {
      "/api": {
        target: "http://127.0.0.1:8081",
        ws: false,
        changOrigin: true,
        pathRewrite: {
          "^/api": "/",
        },
      },
    },
  },
};
