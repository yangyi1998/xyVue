import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import i18n from "./i18n";
import "./assets/icon/iconfont.css";
/*引入全局变量
 * 使用 {{common.apiUrl}}、this.common.apiUrl
 * */
import common from "./config/common";
Vue.prototype.common = common;
/*图标选择器*/
import eIconPicker from "e-icon-picker";
import "e-icon-picker/lib/symbol.js"; //基本彩色图标库
import "e-icon-picker/lib/index.css"; // 基本样式，包含基本图标
import "font-awesome/css/font-awesome.min.css"; //font-awesome 图标库
Vue.use(eIconPicker, {
  FontAwesome: true,
  ElementUI: true,
  eIcon: true,
  eIconSymbol: true,
});
// 自定义表格工具组件
import TableTool from "@/components/table/TableTool.vue";
import Pagination from "@/components/table/Pagination.vue";
import DictTag from "@/components/dictTag";
import Editor from "@/components/editor";

/*echarts*/
import * as echarts from "echarts";
Vue.prototype.$echarts = echarts;
/*mock*/
if (process.env.NODE_ENV === "development") {
  require("@/mock/index.js");
}
/*el-table 动态设置高度*/
import adaptive from "./directive/el-table";
Vue.use(adaptive);

import dict from "@/components/dictData"; // 数据字典
Vue.use(dict);

// 全局组件挂载
Vue.component("TableTool", TableTool);
Vue.component("Pagination", Pagination);
Vue.component("DictTag", DictTag);
Vue.component("Editor", Editor);

Vue.use(ElementUI);
/*关闭浏览器控制台提示*/
Vue.config.productionTip = false;
/*实例化*/
new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app"); /*手动挂载*/
