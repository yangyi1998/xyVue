import Mock from "mockjs";
import { pagination } from "@/utils/mockUtil";

let list = [];
const count = 60;

for (let i = 0; i < count; i++) {
  list.push(
    Mock.mock({
      id: Mock.Random.id(),
      "status|1": ["0", "1"],
      sort: Mock.Random.increment(),
      roleName: Mock.Random.cname(),
      roleCode: Mock.Random.word(5),
      remark: Mock.Random.cparagraph(1, 3),
      createTime: Mock.Random.datetime(),
    })
  );
}
// 设置延时时间
Mock.setup({
  timeout: "300-600",
});

export function getRoleSelect() {
  //实现对拦截到请求的处理
  Mock.mock("/api/role/getRoleSelect", "get", (req) => {
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      data: [
        { label: "管理员", value: "1" },
        { label: "测试", value: "2" },
      ],
    };
  });
}

export function getRoleList() {
  //实现对拦截到请求的处理
  Mock.mock("/api/role/list", "post", (req) => {
    const { roleName, page = 1, limit = 10 } = JSON.parse(req.body);
    const mockList = list.filter((item) => {
      if (roleName && item.roleName.indexOf(roleName) === -1) return false;
      return true;
    });
    const pageList = pagination(page, limit, mockList);
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      count: mockList.length,
      data: pageList,
    };
  });
}
export function updateRoleById() {
  //实现对拦截到请求的处理
  Mock.mock("/api/role/updateRoleById", "post", (req) => {
    let { id, status } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "更改成功~",
      success: true,
      data: null,
    };
  });
}
export function saveRole() {
  //实现对拦截到请求的处理
  Mock.mock("/api/role/saveRole", "post", (req) => {
    let obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: !obj.id ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}
export function saveRoleSelectMenu() {
  //实现对拦截到请求的处理
  Mock.mock("/api/role/saveRoleSelectMenu", "post", (req) => {
    let obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: "授权成功~",
      success: true,
      data: obj,
    };
  });
}
export function deleteRole() {
  //实现对拦截到请求的处理
  Mock.mock("/api/role/delete", "post", (req) => {
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: JSON.parse(req.body),
    };
  });
}
