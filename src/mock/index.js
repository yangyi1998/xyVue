import {
  login,
  getUserList,
  captcha,
  saveUser,
  updateUserById,
  resetPassword,
  deleteUser,
  getInfo,
  logout,
} from "@/mock/user";
import {
  getRoleSelect,
  getRoleList,
  updateRoleById,
  saveRole,
  deleteRole,
  saveRoleSelectMenu,
} from "@/mock/role";
import { getDeptTree, saveDept, deleteDept } from "@/mock/dept";
import { getMenuSelect, getMenuList, saveMenu, deleteMenu } from "@/mock/menu";
import {
  getDictList,
  getDicts,
  saveDict,
  deleteDict,
  getDictDataList,
  saveDictData,
  deleteDictData,
} from "@/mock/dict";
import { getServer } from "@/mock/server";
import { getLogList, deleteLog } from "@/mock/log";
import {
  getNoticeList,
  updateNoticeById,
  saveNotice,
  deleteNotice,
} from "@/mock/notice";
login();
getUserList();
updateUserById();
deleteUser();
resetPassword();
captcha();
saveUser();
getInfo();
logout();
getRoleSelect();
getDeptTree();
saveDept();
deleteDept();
getRoleList();
updateRoleById();
saveRole();
deleteRole();
getMenuSelect();
getMenuList();
deleteMenu();
saveMenu();
saveRoleSelectMenu();
getDictList();
saveDict();
deleteDict();
getDictDataList();
getDicts();
saveDictData();
deleteDictData();
getServer();
getLogList();
deleteLog();
getNoticeList();
updateNoticeById();
saveNotice();
deleteNotice();
