import Mock from "mockjs";
// 设置延时时间
Mock.setup({
  timeout: "300-600",
});

export function getDeptTree() {
  Mock.mock("/api/dept/getDeptTree", "post", (req) => {
    const { deptName } = JSON.parse(req.body);
    let TreeList = [
      {
        deptName: "电务部",
        path: "-1-",
        level: 1,
        createTime: "2022-04-12 12:12:15",
        children: [
          {
            deptName: "成都电务段",
            path: "-1-2-",
            level: 2,
            createTime: "2022-04-12 12:15:15",
            children: [
              {
                deptName: "成都信号车间",
                path: "-1-2-4-",
                level: 3,
                createTime: "2022-04-12 12:15:15",
                deptAllName: "成都信号车间",
                pyName: "CDXHCJ",
                id: 4,
                deptType: "车间",
                sort: "1",
                parentId: 2,
                deptNo: "NO4",
              },
            ],
            deptAllName: "成都电务段",
            pyName: "CDDWD",
            id: 2,
            deptType: "段",
            sort: "1",
            parentId: 1,
            deptNo: "NO2",
          },
          {
            deptName: "重庆电务段",
            path: "-1-3-",
            level: 2,
            createTime: "2022-04-12 12:15:15",
            deptAllName: "重庆电务段",
            pyName: "CQDWD",
            id: 3,
            deptType: "段",
            sort: "1",
            parentId: 1,
            deptNo: "NO3",
          },
          {
            deptName: "贵阳电务段",
            path: "-1-5-",
            level: 2,
            createTime: "2022-04-12 12:15:15",
            deptAllName: "贵阳电务段",
            pyName: "GYDWD",
            id: 5,
            deptType: "段",
            sort: "1",
            parentId: 1,
            deptNo: "NO5",
          },
        ],
        deptAllName: "电务部",
        pyName: "DWB",
        id: 1,
        deptType: "局",
        sort: "1",
        parentId: 0,
        deptNo: "NO1",
      },
    ];
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      data: TreeList,
    };
  });
}

export function saveDept() {
  Mock.mock("/api/dept/saveDept", "post", (req) => {
    const obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: !obj.id ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}

export function deleteDept() {
  Mock.mock("/api/dept/delete", "post", (req) => {
    const { ids } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: null,
    };
  });
}
