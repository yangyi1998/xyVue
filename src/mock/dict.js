import Mock from "mockjs";
import { pagination } from "@/utils/mockUtil";

let list = [
  {
    createTime: "2021-09-09 17:26:50",
    remark: "用户性别列表",
    id: 1,
    dictName: "用户性别",
    dictCode: "sys_user_sex",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:50",
    remark: "菜单状态列表",
    id: 2,
    dictName: "菜单状态",
    dictCode: "sys_show_hide",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:50",
    remark: "系统开关列表",
    id: 3,
    dictName: "系统开关",
    dictCode: "sys_normal_disable",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:51",
    remark: "任务状态列表",
    id: 4,
    dictName: "任务状态",
    dictCode: "sys_job_status",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:51",
    remark: "任务分组列表",
    id: 5,
    dictName: "任务分组",
    dictCode: "sys_job_group",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:51",
    remark: "系统是否列表",
    id: 6,
    dictName: "系统是否",
    dictCode: "sys_yes_no",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:52",
    remark: "通知类型列表",
    id: 7,
    dictName: "通知类型",
    dictCode: "sys_notice_type",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:52",
    remark: "通知状态列表",
    id: 8,
    dictName: "通知状态",
    dictCode: "sys_notice_status",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:52",
    remark: "操作类型列表",
    id: 9,
    dictName: "操作类型",
    dictCode: "sys_oper_type",
    status: "0",
  },
  {
    createTime: "2021-09-09 17:26:52",
    remark: "登录状态列表",
    id: 10,
    dictName: "系统状态",
    dictCode: "sys_common_status",
    status: "0",
  },
];

let rightList = [
  {
    id: 1,
    dictId: 10,
    sort: 1,
    dictDataName: "成功",
    dictDataCode: 0,
    remark: "正常状态",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "primary",
    cssClass: "",
  },
  {
    id: 2,
    dictId: 10,
    sort: 2,
    dictDataName: "失败",
    dictDataCode: 1,
    remark: "停用状态",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "primary",
    cssClass: "",
  },
  {
    id: 3,
    dictId: 9,
    sort: 1,
    dictDataName: "添加",
    dictDataCode: 1,
    remark: "添加操作",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "primary",
    cssClass: "",
  },
  {
    id: 4,
    dictId: 9,
    sort: 2,
    dictDataName: "修改",
    dictDataCode: 2,
    remark: "修改操作",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "warning",
    cssClass: "",
  },
  {
    id: 5,
    dictId: 9,
    sort: 3,
    dictDataName: "删除",
    dictDataCode: 3,
    remark: "删除操作",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "danger",
    cssClass: "",
  },
  {
    id: 6,
    dictId: 7,
    sort: 1,
    dictDataName: "公告",
    dictDataCode: 1,
    remark: "公告",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "primary",
    cssClass: "",
  },
  {
    id: 7,
    dictId: 7,
    sort: 2,
    dictDataName: "通知",
    dictDataCode: 2,
    remark: "通知",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "warning",
    cssClass: "",
  },
  {
    id: 8,
    dictId: 8,
    sort: 1,
    dictDataName: "正常",
    dictDataCode: 0,
    remark: "正常",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "primary",
    cssClass: "",
  },
  {
    id: 9,
    dictId: 8,
    sort: 2,
    dictDataName: "关闭",
    dictDataCode: 1,
    remark: "关闭",
    createTime: "2021-09-09 17:26:52",
    updateTime: "2021-09-09 17:26:52",
    listClass: "primary",
    cssClass: "",
  },
];
let rightListConvert = [
  {
    dictLabel: "成功",
    dictValue: 0,
    dictCode: "sys_common_status",
    listClass: "primary",
    cssClass: "",
  },
  {
    dictLabel: "失败",
    dictCode: "sys_common_status",
    dictValue: 1,
    listClass: "danger",
    cssClass: "",
  },
  {
    dictLabel: "正常",
    dictValue: 0,
    dictCode: "sys_notice_status",
    listClass: "primary",
    cssClass: "",
  },
  {
    dictLabel: "关闭",
    dictCode: "sys_notice_status",
    dictValue: 1,
    listClass: "danger",
    cssClass: "",
  },
  {
    dictLabel: "添加",
    dictValue: 1,
    dictCode: "sys_oper_type",
    listClass: "primary",
    cssClass: "",
  },
  {
    dictLabel: "修改",
    dictValue: 2,
    dictCode: "sys_oper_type",
    listClass: "primary",
    cssClass: "",
  },
  {
    dictLabel: "删除",
    dictValue: 3,
    dictCode: "sys_oper_type",
    listClass: "danger",
    cssClass: "",
  },
  {
    dictLabel: "生成代码",
    dictValue: 8,
    dictCode: "sys_oper_type",
    listClass: "warning",
    cssClass: "",
  },
  {
    dictLabel: "公告",
    dictValue: 1,
    dictCode: "sys_notice_type",
    listClass: "primary",
    cssClass: "",
  },
  {
    dictLabel: "通知",
    dictValue: 2,
    dictCode: "sys_notice_type",
    listClass: "warning",
    cssClass: "",
  },
];
// 设置延时时间
Mock.setup({
  timeout: "300-600",
});

export function getDictList() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dict/list", "post", (req) => {
    const { page = 1, limit = 10 } = JSON.parse(req.body);
    const mockList = list.filter((item) => {
      return true;
    });
    const pageList = pagination(page, limit, mockList);
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      count: mockList.length,
      data: pageList,
    };
  });
}
export function saveDict() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dict/saveDict", "post", (req) => {
    let obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: !obj.id ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}

export function getDictDataList() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dictData/list", "post", (req) => {
    const {
      page = 1,
      limit = 10,
      dictId,
      dictDataName,
      dictDataCode,
    } = JSON.parse(req.body);
    const mockList = rightList.filter((item) => {
      if (
        (dictId && item.dictId.toString() !== dictId.toString()) ||
        (dictDataName && item.dictDataName.indexOf(dictDataName) === -1) ||
        (dictDataCode && item.dictDataCode.indexOf(dictDataCode) === -1)
      )
        return false;
      return true;
    });
    const pageList = pagination(page, limit, mockList);
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      count: mockList.length,
      data: pageList,
    };
  });
}
export function getDicts() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dictData/getDicts", "post", (req) => {
    const dictCode = req.body;
    const mockList = rightListConvert.filter((item) => {
      if (dictCode && item.dictCode !== dictCode) return false;
      return true;
    });
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      data: mockList,
    };
  });
}
export function deleteDict() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dict/delete", "post", (req) => {
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: JSON.parse(req.body),
    };
  });
}

export function saveDictData() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dictData/saveDictData", "post", (req) => {
    let obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: !obj.id ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}
export function deleteDictData() {
  //实现对拦截到请求的处理
  Mock.mock("/api/dictData/delete", "post", (req) => {
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: JSON.parse(req.body),
    };
  });
}
