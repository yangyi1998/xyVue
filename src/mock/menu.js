import Mock from "mockjs";
// 设置延时时间
Mock.setup({
  timeout: "300-600",
});

export function getMenuList() {
  Mock.mock("/api/menu/list", "post", (req) => {
    let TreeList = [
      {
        id: 1,
        menuName: "系统管理",
        icon: "el-icon-setting",
        parentId: 0,
        path: "",
        component: "",
        target: 1,
        permission: "",
        sort: 1,
        status: 0,
        isShow: 0,
        type: 0,
        createTime: "2022-04-26 10:50:20",
        children: [
          {
            id: 2,
            parentId: 1,
            icon: "el-icon-user-solid",
            menuName: "部门管理",
            path: "system/dept/index",
            component: "system/dept/index",
            target: 1,
            permission: "",
            sort: 2,
            status: 0,
            isShow: 0,
            type: 1,
            createTime: "2022-04-26 10:50:20",
            children: [
              {
                id: 8,
                parentId: 2,
                icon: "",
                menuName: "添加",
                path: "",
                isShow: 0,
                component: "",
                target: 1,
                permission: "system:dept:add",
                sort: 8,
                status: 0,
                type: 2,
                createTime: "2022-04-26 10:50:20",
              },
              {
                id: 9,
                parentId: 2,
                icon: "",
                menuName: "修改",
                path: "",
                component: "",
                target: 1,
                permission: "system:dept:edit",
                sort: 9,
                status: 0,
                type: 2,
                createTime: "2022-04-26 10:50:20",
              },
              {
                id: 10,
                parentId: 2,
                icon: "",
                menuName: "删除",
                path: "",
                component: "",
                target: 1,
                permission: "system:dept:delete",
                sort: 10,
                status: 0,
                type: 2,
                createTime: "2022-04-26 10:50:20",
              },
            ],
          },
          {
            id: 3,
            parentId: 1,
            icon: "el-icon-user",
            menuName: "用户管理",
            children: [
              {
                id: 11,
                parentId: 3,
                icon: "",
                menuName: "添加",
              },
              {
                id: 12,
                parentId: 3,
                icon: "",
                menuName: "修改",
              },
              {
                id: 13,
                parentId: 3,
                icon: "",
                menuName: "删除",
              },
            ],
          },
          {
            id: 4,
            parentId: 1,
            icon: "el-icon-postcard",
            menuName: "角色管理",
            children: [],
          },
          {
            id: 5,
            parentId: 1,
            icon: "el-icon-date",
            menuName: "菜单管理",
            children: [],
          },
          {
            id: 6,
            parentId: 1,
            icon: "el-icon-notebook-1",
            menuName: "字典管理",
            children: [],
          },
          {
            id: 7,
            parentId: 1,
            icon: "el-icon-reading",
            menuName: "日志管理",
            children: [],
          },
        ],
      },
    ];
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      data: TreeList,
    };
  });
}
export function getMenuSelect() {
  Mock.mock("/api/menu/getMenuSelect", "post", (req) => {
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      data: "9,10",
    };
  });
}
export function saveMenu() {
  Mock.mock("/api/menu/saveMenu", "post", (req) => {
    const obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: !obj.id ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}

export function deleteMenu() {
  Mock.mock("/api/menu/delete", "post", (req) => {
    const { ids } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: null,
    };
  });
}
