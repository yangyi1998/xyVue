import Mock from "mockjs";
import { pagination } from "@/utils/mockUtil";

let list = [
  {
    browse: 0,
    createBy: "admin",
    createTime: "2021-09-09 17:27:23",
    updateBy: "",
    updateTime: null,
    remark: "管理员",
    id: 1,
    noticeTitle: "温馨提醒：2018-07-01 若依新版本发布啦",
    noticeType: 2,
    noticeContent: "<p>Ajax 异步设置内容 HTML</p>",
    status: "0",
  },
  {
    browse: 0,
    createBy: "admin",
    createTime: "2021-09-09 17:27:23",
    updateBy: "",
    updateTime: null,
    remark: "管理员",
    id: 2,
    noticeTitle: "维护通知：2018-07-01 若依系统凌晨维护",
    noticeType: 1,
    noticeContent: "<p>Ajax 异步设置内容 HTML222</p>",
    status: "1",
  },
];
// 设置延时时间
Mock.setup({
  timeout: "300-600",
});

export function getNoticeList() {
  //实现对拦截到请求的处理
  Mock.mock("/api/notice/list", "post", (req) => {
    const {
      noticeTitle,
      createBy,
      noticeType,
      page = 1,
      limit = 10,
    } = JSON.parse(req.body);
    const mockList = list.filter((item) => {
      if (
        (noticeTitle && item.noticeTitle.indexOf(noticeTitle) === -1) ||
        (createBy && item.createBy.indexOf(createBy) === -1) ||
        (noticeType && item.noticeType !== noticeType)
      )
        return false;
      return true;
    });
    const pageList = pagination(page, limit, mockList);
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      count: mockList.length,
      data: pageList,
    };
  });
}
export function updateNoticeById() {
  //实现对拦截到请求的处理
  Mock.mock("/api/notice/updateNoticeById", "post", (req) => {
    let { id, status } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "更改成功~",
      success: true,
      data: null,
    };
  });
}
export function saveNotice() {
  //实现对拦截到请求的处理
  Mock.mock("/api/notice/saveNotice", "post", (req) => {
    let obj = JSON.parse(req.body);
    return {
      code: 200,
      msg: !obj.id ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}
export function deleteNotice() {
  //实现对拦截到请求的处理
  Mock.mock("/api/notice/delete", "post", (req) => {
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: JSON.parse(req.body),
    };
  });
}
