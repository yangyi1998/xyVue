import Mock from "mockjs";
import { pagination } from "@/utils/mockUtil";

let list = [];
const count = 60;

for (let i = 0; i < count; i++) {
  list.push(
    Mock.mock({
      id: Mock.Random.id(),
      username: Mock.Random.cname(),
      account: Mock.Random.string(6),
      birthday: Mock.Random.date(),
      "headImg|1": [
        "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F14099261836%2F1000.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652240531&t=08e25affe8c6dbc0c879211f24ae1f7b",
        "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F13495054124%2F1000.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652240531&t=640139b36fc47b7a3e6c49d2f2e54f4b",
        "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fi.qqkou.com%2Fi%2F3a2393620006x2972640609b26.jpg&refer=http%3A%2F%2Fi.qqkou.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1653185926&t=ca57ccf67f4c6dbf705ab0f7a8b1d5a4",
      ],
      "sex|1": ["男", "女"],
      "status|1": ["0", "1"],
      "deptId|1": [1, 2],
      "deptName|1": ["成都扛把子", "大邑县街溜子"],
      "roleNames|1": ["管理员", "测试"],
      "roleIds|1": [1, 2],
      email: Mock.Random.email(),
      phone: /^1[385][1-9]\d{8}/,
      createTime: Mock.Random.datetime(),
    })
  );
}
// 设置延时时间
Mock.setup({
  timeout: "300-600",
});
export function login() {
  Mock.mock("/api/login", "post", (req) => {
    let { username, password, captcha } = JSON.parse(req.body);
    if (username === "xyVue" && password === "111111" && captcha === "520") {
      return {
        code: 200,
        msg: "登录成功~",
        success: true,
        data: {
          token: "teosdsfdfdksdfs232323",
          userInfo: {
            headImg:
              "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F14099261836%2F1000.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652240531&t=08e25affe8c6dbc0c879211f24ae1f7b",
            account: "xyVye",
            userName: "xyVye",
            deptId: "1",
            deptNo: "xyVue",
            deptName: "部门哥老官",
            roleId: "1",
            roleName: "超级管理员",
          },
        },
      };
    } else {
      return {
        code: 500,
        msg: "账号密码错误~",
        success: false,
      };
    }
  });
}
export function captcha() {
  //实现对拦截到请求的处理
  Mock.mock("/api/captcha", "post", (req) => {
    return Mock.Random.dataImage(
      "50x20",
      Mock.mock({ regexp: /\w{4}/ }).regexp
    );
  });
}
export function getUserList() {
  //实现对拦截到请求的处理
  Mock.mock("/api/user/list", "post", (req) => {
    const {
      username,
      sex,
      account,
      page = 1,
      limit = 10,
    } = JSON.parse(req.body);
    const mockList = list.filter((user) => {
      if (
        (username && user.username.indexOf(username) === -1) ||
        (sex && user.sex.indexOf(sex) === -1) ||
        (account && user.account.indexOf(account) === -1)
      )
        return false;
      return true;
    });
    const pageList = pagination(page, limit, mockList);
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      count: mockList.length,
      data: pageList,
    };
  });
}
export function saveUser() {
  //实现对拦截到请求的处理
  Mock.mock("/api/user/saveUser", "post", (req) => {
    return {
      code: 200,
      msg: !req.body.get("id") ? "添加成功~" : "修改成功~",
      success: true,
      data: null,
    };
  });
}
export function updateUserById() {
  //实现对拦截到请求的处理
  Mock.mock("/api/user/updateUserById", "post", (req) => {
    let { id, status } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "更改成功~",
      success: true,
      data: null,
    };
  });
}
export function resetPassword() {
  //实现对拦截到请求的处理
  Mock.mock("/api/user/resetPassword", "post", (req) => {
    let { id } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "重置成功~",
      success: true,
      data: null,
    };
  });
}
export function deleteUser() {
  //实现对拦截到请求的处理
  Mock.mock("/api/user/delete", "post", (req) => {
    let { ids } = JSON.parse(req.body);
    return {
      code: 200,
      msg: "删除成功~",
      success: true,
      data: null,
    };
  });
}
export function logout() {
  //实现对拦截到请求的处理
  Mock.mock("/api/logout", "post", (req) => {
    return {
      code: 200,
      msg: "退出成功~",
      success: true,
      data: null,
    };
  });
}
export function getInfo() {
  Mock.mock("/api/user/getInfo", "post", (req) => {
    return {
      code: 200,
      msg: "登录成功~",
      success: true,
      data: {
        token: "teosdsfdfdksdfs232323",
        user: {
          headImg:
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_bt%2F0%2F14099261836%2F1000.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652240531&t=08e25affe8c6dbc0c879211f24ae1f7b",
          account: "xyVye",
          userName: "xyVye",
          deptId: "1",
          deptNo: "xyVue",
          deptName: "部门哥老官",
        },
        roles: "超级管理员",
        permissions: "",
      },
    };
  });
}
