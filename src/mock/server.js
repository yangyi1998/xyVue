import Mock from "mockjs";

// 设置延时时间
Mock.setup({
  timeout: "300-600",
});

export function getServer() {
  //实现对拦截到请求的处理
  Mock.mock("/api/monitor/server", "post", (req) => {
    return {
      code: 200,
      msg: "获取成功~",
      success: true,
      data: {
        cpuNum: 16,
        free: 50.0,
        jvmInfoFree: 418.56,
        jvmInfoHome: "D:\\StudyToolsInstall\\Java\\jdk1.8.0_181\\jre",
        jvmInfoMax: 3621.5,
        jvmInfoName: "Java HotSpot(TM) 64-Bit Server VM",
        jvmInfoRunTime: "0天0小时4分钟",
        jvmInfoStartTime: "2022-05-29 21:22:43",
        jvmInfoTotal: 3092.0,
        jvmInfoVersion: "1.8.0_181",
        jvmUsage: 86.46,
        jvmUsed: 2673.44,
        memInfo: {
          free: 2.96,
          total: 15.91,
          usage: 81.38,
          used: 12.95,
        },
        sys: 108.0,
        sysFiles: [
          {
            dirName: "C:\\",
            free: "112.0 GB",
            sysTypeName: "NTFS",
            total: "200.0 GB",
            typeName: "本地固定磁盘 (C:)",
            usage: 44.02,
            used: "88.0 GB",
          },
          {
            dirName: "D:\\",
            free: "341.7 GB",
            sysTypeName: "NTFS",
            total: "366.0 GB",
            typeName: "本地固定磁盘 (D:)",
            usage: 6.65,
            used: "24.3 GB",
          },
          {
            dirName: "E:\\",
            free: "262.7 GB",
            sysTypeName: "NTFS",
            total: "365.1 GB",
            typeName: "本地固定磁盘 (E:)",
            usage: 28.06,
            used: "102.4 GB",
          },
        ],
        sysInfoComputerIp: "192.168.1.151",
        sysInfoComputerName: "DESKTOP-O374V8B",
        sysInfoOsArch: "amd64",
        sysInfoOsName: "Windows 10",
        sysInfoUserDir: "D:\\studyWork\\ideaWork_2020\\permissions_general",
        total: 5250.0,
        used: 125.0,
        wait: 1.0,
      },
    };
  });
}
