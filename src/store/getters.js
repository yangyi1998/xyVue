const getters = {
  token: (state) => state.user.token,
  userInfo: (state) => state.user.userInfo,
  roles: (state) => state.user.roles,
  permissions: (state) => state.user.permissions,
  language: (state) => state.language.language,
};
export default getters;
