import { setToken, removeToken } from "@/utils/auth";
import { Encrypt, deepClone, Decrypt } from "@/utils/xiaoyi";
const language = {
  state: {
    language: localStorage.getItem("language") || "zh",
  },
  actions: {
    setLanguage({ commit }, language) {
      commit("SET_LANGUAGE", language);
    },
  },
  mutations: {
    SET_LANGUAGE: (state, language) => {
      state.language = language;
      localStorage.setItem("language", language);
    },
  },
};
export default language;
