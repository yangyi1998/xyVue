import { login, logout, getInfo } from "@/api/system/login";
import { getToken, setToken, removeToken } from "@/utils/auth";
import { Encrypt, deepClone, Decrypt } from "@/utils/xiaoyi";
const user = {
  state: {
    token: getToken(),
    userInfo: {},
    roles: [],
    permissions: [],
  },
  actions: {
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo)
          .then((res) => {
            setToken(res.data.token);
            commit("SET_TOKEN", res.data.token);
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.userInfo)
          .then((res) => {
            const userInfo = res.data.user;
            userInfo.headImg =
              userInfo.headImg == "" || userInfo.headImg == null
                ? require("@/assets/images/login/headImage.jpg")
                : userInfo.headImg;
            if (res.data.roles && res.data.roles.length > 0) {
              // 验证返回的roles是否是一个非空数组
              commit("SET_ROLES", res.data.roles);
              commit("SET_PERMISSIONS", res.data.permissions);
            } else {
              commit("SET_ROLES", ["ROLE_DEFAULT"]);
            }
            commit("SET_USER_INFO", userInfo);
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    }, // 退出系统
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token)
          .then(() => {
            commit("SET_TOKEN", "");
            commit("SET_ROLES", []);
            commit("SET_PERMISSIONS", []);
            removeToken();
            window.sessionStorage.clear();
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_USER_INFO: (state, userInfo) => {
      state.userInfo = userInfo;
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions;
    },
  },
};
export default user;
