import Vue from "vue";
import vuex from "vuex";
import user from "@/store/modules/user";
import language from "@/store/modules/language";
import getters from "@/store/getters";
Vue.use(vuex);
const store = new vuex.Store({
  modules: {
    user,
    language,
  },
  getters,
});

export default store;
