

export function pagination(page, limit, list) {
    return list.slice((page - 1) * limit, page * limit);
}
