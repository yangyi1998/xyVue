import Cookies from "js-cookie";
import Common from "@/config/common";

const TokenKey = Common.Authorization;
let inFifteenMinutes = new Date(new Date().getTime() + Common.tokenTime * 1000);

export function getToken() {
  return Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token, { expires: inFifteenMinutes });
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}
