const BASE_URL = process.env.NODE_ENV === "production" ? "" : "";
export default {
  /**
   * @description api请求基础路径
   */
  baseURL: BASE_URL,

  apiUrl: BASE_URL + "/api/",
  /**
   * @description 图片请求路径
   */
  imgURL: "http:127.0.0.1:8888/xy_vue/",

  /**
   * @description 配置主键,目前用于存储
   */
  key: "xyVue",
  /**
   * @description Token
   */
  Authorization: "xyVue-Token",
  /**
   * @description token过期时间
   */
  tokenTime: 6000,
  /**
   * @description 版权信息
   */
  copyright: "copyright © 2021 all rights reserved. xy_vue 1.1.0",
};
