import Vue from "vue";
import VueRouter from "vue-router";
import NProgress from "nprogress"; //引入nprogress
import "nprogress/nprogress.css"; //这个样式必须引入
import { getToken } from "@/utils/auth";
import { isRelogin } from "@/utils/request";
import { Notification } from "element-ui";
import store from "@/store";

/*设置nprogress页面加载进度条*/
NProgress.configure({ showSpinner: false });

/*安装路由*/
Vue.use(VueRouter);

// const originalPush = VueRouter.prototype.push;
// VueRouter.prototype.push = function push(location) {
//     return originalPush.call(this, location).catch(err => err)
// };
// const originalReplace = VueRouter.prototype.replace;
// VueRouter.prototype.replace = function replace(location) {
//     return originalReplace.call(this, location).catch(err => err)
// };
/*
 * 配置路由
 *   注：path必填不然菜单会出现影响；如：展开和关闭会互相影响
 * */
const routes = [
  {
    path: "/401",
    component: () => import("@/views/error-page/401"),
    name: "401",
    hide: true,
  },
  {
    path: "/500",
    component: () => import("@/views/error-page/500"),
    name: "500",
    hide: true,
  },
  {
    path: "*",
    component: () => import("@/views/error-page/404"),
    name: "404",
    hide: true,
  },
  {
    path: "/login",
    component: () => import("@/views/login/login.vue"),
    /*懒加载 按需加载*/
    // component: () => resolve => require(['@/views/login/login.vue'], resolve),
    name: "登录",
    hide: true,
  },
  {
    path: "/register",
    component: () => import("@/views/login/Register.vue"),
    name: "注册",
    hide: true,
  },
  {
    path: "/forgetPassword",
    component: () => import("@/views/login/ForgetPassword.vue"),
    name: "找回密码",
    hide: true,
  },
  {
    path: "/",
    component: () => import("@/views/home/home.vue"),
    name: "",
    hide: true,
    redirect: "/home",
    children: [
      {
        path: "/home",
        component: () => import("@/views/home/welcome.vue"),
        icon: "el-icon-monitor",
        hide: true,
        name: "首页",
      },
      {
        path: "/system",
        icon: "el-icon-setting",
        name: "系统管理",
        component: {
          render(h) {
            return h("router-view");
          },
        },
        children: [
          {
            path: "/dept",
            icon: "el-icon-user-solid",
            component: () => import("@/views/system/dept/index.vue"),
            name: "部门管理",
          },
          {
            path: "/user",
            icon: "el-icon-user",
            component: () => import("@/views/system/user/index.vue"),
            name: "用户管理",
          },
          {
            path: "/role",
            icon: "el-icon-postcard",
            component: () => import("@/views/system/role/index.vue"),
            name: "角色管理",
          },
          {
            path: "/menu",
            icon: "el-icon-date",
            component: () => import("@/views/system/menu/index.vue"),
            name: "菜单管理",
          },
          {
            path: "/dict",
            icon: "el-icon-notebook-1",
            component: () => import("@/views/system/dict/index.vue"),
            name: "字典管理",
          },
          {
            path: "/notice",
            icon: "el-icon-chat-dot-round",
            component: () => import("@/views/system/notice/index.vue"),
            name: "通知公告",
          },
          {
            path: "/log",
            icon: "el-icon-reading",
            name: "日志管理",
            component: () => import("@/components/parentView/index.vue"),
            children: [
              {
                path: "/otherIndex",
                component: () => import("@/views/system/log/otherIndex.vue"),
                name: "操作日志",
              },
              {
                path: "/loginIndex",
                component: () => import("@/views/system/log/loginIndex.vue"),
                name: "登录日志",
              },
            ],
          },
        ],
      },
      {
        path: "/environment",
        name: "系统环境",
        icon: "el-icon-sunny",
        component: {
          render(h) {
            return h("router-view");
          },
        },
        children: [
          {
            path: "/druid",
            component: () => import("@/views/monitor/druid/index.vue"),
            name: "Druid监控",
          },
          {
            path: "/server",
            component: () => import("@/views/monitor/server/index.vue"),
            name: "服务监控",
          },
        ],
      },
    ],
  },
];
const router = new VueRouter({
  //vue-router默认是hash模式的，我们要切换到history模式，只需在router初始化时，设置mode为history即可
  mode: "history",
  routes,
});
const whiteList = [
  "/login",
  "/401",
  "/404",
  "/500",
  "/register",
  "/forgetPassword",
];
router.beforeEach((to, from, next) => {
  NProgress.start();
  if (getToken()) {
    document.title = to.name + " - xy Vue";
    /* has token*/
    if (to.path === "/login") {
      next({ path: "/" });
      NProgress.done();
    } else {
      if (store.getters.roles.length === 0) {
        isRelogin.show = true;
        // 判断当前用户是否已拉取完user_info信息
        store
          .dispatch("GetInfo")
          .then(() => {
            isRelogin.show = false;
            next();
          })
          .catch((err) => {
            store.dispatch("LogOut").then(() => {
              Notification.error({
                message: err,
                duration: 2 * 1000,
              });
              next({ path: "/" });
            });
          });
      } else {
        next();
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next();
    } else {
      next(`/login?redirect=${to.fullPath}`); // 否则全部重定向到登录页
      NProgress.done();
    }
  }
});
router.afterEach(() => {
  NProgress.done();
});

export default router;
