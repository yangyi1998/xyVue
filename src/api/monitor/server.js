import common from "@/config/common";
import request from "@/utils/request";

export const getServer = (data) => {
  return request({
    url: common.apiUrl + "monitor/server",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};
