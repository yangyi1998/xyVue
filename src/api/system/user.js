import common from "@/config/common";
import request from "@/utils/request";

/*获取用户列表*/
export function getUserList(data) {
  return request({
    url: common.apiUrl + "user/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
}

/*添加用户*/
//https://www.jianshu.com/p/5025c48ba2f4
export function saveUser(data) {
  return request({
    url: common.apiUrl + "user/saveUser",
    method: "post",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data: data,
  });
}
/*修改状态*/
export function updateUserById(data) {
  return request({
    url: common.apiUrl + "user/updateUserById",
    method: "post",
    data: data,
  });
}

/*重置密码*/
export function resetPassword(data) {
  return request({
    url: common.apiUrl + "user/resetPassword",
    method: "post",
    data: data,
  });
}

/*删除用户*/
export function deleteUser(data) {
  return request({
    url: common.apiUrl + "user/delete",
    method: "post",
    data: data,
  });
}
