import common from "@/config/common";
import request from "@/utils/request";

/*获取角色下拉框*/
export const getRoleSelect = (data) => {
  return request({
    url: common.apiUrl + "role/getRoleSelect",
    method: "get",
    data: data,
  });
};
/*角色列表*/
export const getRoleList = (data) => {
  return request({
    url: common.apiUrl + "role/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};
/*修改状态*/
export function updateRoleById(data) {
  return request({
    url: common.apiUrl + "role/updateRoleById",
    method: "post",
    data: data,
  });
}
/*角色添加*/
export function saveRole(data) {
  return request({
    url: common.apiUrl + "role/saveRole",
    method: "post",
    data: data,
  });
}
/*角色授权*/
export function saveRoleSelectMenu(data) {
  return request({
    url: common.apiUrl + "role/saveRoleSelectMenu",
    method: "post",
    data: data,
  });
}

/*角色删除*/
export function deleteRole(data) {
  return request({
    url: common.apiUrl + "role/delete",
    method: "post",
    data: data,
  });
}
