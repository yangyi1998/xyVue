import common from "@/config/common";
import request from "@/utils/request";

// 登录方法
export function login(data) {
  return request({
    url: common.apiUrl + "login",
    method: "post",
    data: data,
  });
}
export function captcha(data) {
  return request({
    url: common.apiUrl + "captcha",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
}
export function logout(data) {
  return request({
    url: common.apiUrl + "logout",
    method: "post",
    data: data,
  });
}
export function getInfo(data) {
  return request({
    url: common.apiUrl + "user/getInfo",
    method: "post",
    data: data,
  });
}
