import common from "@/config/common";
import request from "@/utils/request";

/*获取部门列表*/
export const getDeptTree = (data) => {
  return request({
    url: common.apiUrl + "dept/getDeptTree",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};
/*添加部门*/
export const saveDept = (data) => {
  return request({
    url: common.apiUrl + "dept/saveDept",
    method: "post",
    data: data,
  });
};
/*删除部门*/
export const deleteDept = (data) => {
  return request({
    url: common.apiUrl + "dept/delete",
    method: "post",
    data: data,
  });
};
