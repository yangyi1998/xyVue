import common from "@/config/common";
import request from "@/utils/request";
import router from "@/router";

export function getMenuList(data) {
  return request({
    url: common.apiUrl + "menu/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
}

export function getMenuSelect(data) {
  return request({
    url: common.apiUrl + "menu/getMenuSelect",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
}

export const saveMenu = (data) => {
  return request({
    url: common.apiUrl + "menu/saveMenu",
    method: "post",
    data: data,
  });
};

export const deleteMenu = (data) => {
  return request({
    url: common.apiUrl + "menu/delete",
    method: "post",
    data: data,
  });
};

export const initMenu = (router, store) => {
  if (store.state.routes.length > 0) {
    return true;
  }
  request({
    url: common.apiUrl + "login",
    method: "get",
  }).then((response) => {
    if (response && response.data) {
      let fmtRoutes = formatRoutes(data);
    }
  });
};

export const formatRoutes = (routes) => {
  let fmtRoutes = [];
  routes.forEach((router) => {
    let { path, compoent, name, icon, children } = router;
    let fmRouter = {
      path: path,
      name: name,
      icon: icon,
      children: children,
      compoent(resolve) {
        require(["@/views/" + compoent + ".vue"], resolve);
      },
    };
  });
};
