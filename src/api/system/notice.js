import common from "@/config/common";
import request from "@/utils/request";

export const getNoticeList = (data) => {
  return request({
    url: common.apiUrl + "notice/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};

export function updateNoticeById(data) {
  return request({
    url: common.apiUrl + "notice/updateNoticeById",
    method: "post",
    data: data,
  });
}

export function saveNotice(data) {
  return request({
    url: common.apiUrl + "notice/saveNotice",
    method: "post",
    data: data,
  });
}
export function deleteNotice(data) {
  return request({
    url: common.apiUrl + "notice/delete",
    method: "post",
    data: data,
  });
}
