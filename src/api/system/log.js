import common from "@/config/common";
import request from "@/utils/request";
export function getLogList(data) {
  return request({
    url: common.apiUrl + "log/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
}

export function deleteLog(data) {
  return request({
    url: common.apiUrl + "log/delete",
    method: "post",
    data: data,
  });
}
