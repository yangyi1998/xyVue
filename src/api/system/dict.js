import common from "@/config/common";
import request from "@/utils/request";

export const getDictList = (data) => {
  return request({
    url: common.apiUrl + "dict/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};

export function saveDict(data) {
  return request({
    url: common.apiUrl + "dict/saveDict",
    method: "post",
    data: data,
  });
}

export function deleteDict(data) {
  return request({
    url: common.apiUrl + "dict/delete",
    method: "post",
    data: data,
  });
}
export const getDictDataList = (data) => {
  return request({
    url: common.apiUrl + "dictData/list",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};

export const getDicts = (data) => {
  return request({
    url: common.apiUrl + "dictData/getDicts",
    method: process.env.NODE_ENV === "development" ? "post" : "get",
    data: data,
  });
};

export function saveDictData(data) {
  return request({
    url: common.apiUrl + "dictData/saveDictData",
    method: "post",
    data: data,
  });
}

export function deleteDictData(data) {
  return request({
    url: common.apiUrl + "dictData/delete",
    method: "post",
    data: data,
  });
}
